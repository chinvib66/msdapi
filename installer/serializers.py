from rest_framework import serializers
from .models import InstallerModel, InstallationModel, RepairModel, InstallerImage
from duser.models import DomeUserImage
from duser.serializers import DUserImageSerializer, ComplaintSerializer


class InstallerImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstallerImage
        exclude = ['created_at']
        read_only_fields = ['id']


class InstallerSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstallerModel
        # fields = ('__all__')
        exclude = ['created_at', 'updated_at']
        read_only_fields = ['id', 'msduser']


class InstallationSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()

    class Meta:
        model = InstallationModel
        # fields = '__all__'
        exclude = ['created_at', 'updated_at']

    def get_images(self, obj):
        images = InstallerImage.objects.filter(installation=obj)
        serializer = InstallerImageSerializer(images, many=True)
        return serializer.data


class RepairSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()
    # complaint_images = serializers.SerializerMethodField()
    complaint_detail = serializers.SerializerMethodField()

    class Meta:
        model = RepairModel
        # fields = '__all__'
        exclude = ['created_at', 'updated_at', 'complaint']
        read_only = ['complaint']

    def get_images(self, obj):
        images = InstallerImage.objects.filter(repair=obj)
        serializer = InstallerImageSerializer(images, many=True)
        return serializer.data

    def get_complaint_images(self, obj):
        if obj.complaint is None:
            return None
        complaint = obj.complaint
        images = DomeUserImage.objects.filter(complaint=complaint)
        serializer = DUserImageSerializer(images, many=True)
        return serializer.data

    def get_complaint_detail(self, obj):
        if obj.complaint is None:
            return None
        complaint = obj.complaint
        serializer = ComplaintSerializer(complaint)
        return serializer.data
