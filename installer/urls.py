from django.urls import path

from .views import InstallerApi, InstallationApi, RepairApi, ComplaintsInstallerApiView, RepairFromComplaintApiView


app_name = 'installer'
urlpatterns = [
    path('', InstallerApi.as_view()),
    path('installations/',
         InstallationApi.as_view({'get': 'list', 'post': 'create'})),
    path('installations/<pk>/',
         InstallationApi.as_view({'get': 'retrieve', 'patch': 'partial_update'})),
    path('repairs/',
         RepairApi.as_view({'get': 'list', 'post': 'create'})),
    path('repairs/<pk>/',
         RepairApi.as_view({'get': 'retrieve', 'patch': 'partial_update'})),
    path('complaints/',
         ComplaintsInstallerApiView.as_view({'get': 'list'})),
    path('complaints/<pk>',
         ComplaintsInstallerApiView.as_view({'get': 'retrieve'})),
    path('createRepair/',
         RepairFromComplaintApiView.as_view())
]
