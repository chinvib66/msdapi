from rest_framework import generics, permissions, viewsets, status, parsers
from rest_framework.response import Response
from .serializers import InstallationSerializer, InstallerSerializer, RepairSerializer, InstallerImageSerializer
from rest_framework.permissions import IsAuthenticated
from msdapi.permissions import IsInstaller
from .models import InstallerImage, InstallationModel, RepairModel
from duser.models import ComplaintModel
from duser.serializers import ComplaintSerializer


class InstallerApi(generics.RetrieveAPIView):
    serializer_class = InstallerSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self):
        msduser = self.request.user.msduser.get()
        if msduser.utype == 'DI':
            return msduser.installer.get()
        return None


class InstallationApi(viewsets.ModelViewSet):
    serializer_class = InstallationSerializer
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = (parsers.MultiPartParser, parsers.JSONParser,
                      parsers.FormParser, parsers.FileUploadParser)

    def get_queryset(self):
        return self.request.user.msduser.get().installer.get().installations.all()

    def get_installer(self):
        return self.request.user.msduser.get().installer.get()

    def get_images(self, pk):
        return self.request.user.msduser.get().installer.get().installations.get(pk=pk).installation_images.all()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(installer=self.get_installer())
        files = []
        for i in range(int(request.data['fileCount'])):
            print(request.data['file[{0}]'.format(i)])
            files.append({
                'image': request.data['file[{0}]'.format(i)],
                'pid': request.data['pid'],
                'installation': serializer.instance.pk
            })
        images = InstallerImageSerializer(data=files, many=True)
        images.is_valid(raise_exception=True)
        images.save()
        return Response({'installations': serializer.data}, status.HTTP_201_CREATED)


class RepairApi(viewsets.ModelViewSet):
    serializer_class = RepairSerializer
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = (parsers.MultiPartParser, parsers.JSONParser,
                      parsers.FormParser, parsers.FileUploadParser)

    def get_queryset(self):
        return self.request.user.msduser.get().installer.get().repairs.all()

    def get_installer(self):
        return self.request.user.msduser.get().installer.get()

    def get_images(self, pk):
        return self.request.user.msduser.get().installer.get().repairs.get(pk=pk).repair_images.all()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(installer=self.get_installer())
        files = []
        for i in range(int(request.data['fileCount'])):
            print(request.data['file[{0}]'.format(i)])
            files.append({
                'sourceType': 'repair',
                'image': request.data['file[{0}]'.format(i)],
                'pid': request.data['pid'],
                'repair': serializer.instance.pk
            })
        images = InstallerImageSerializer(data=files, many=True)
        images.is_valid(raise_exception=True)
        images.save()
        return Response({'repairs': serializer.data}, status.HTTP_201_CREATED)

    # TODO Update Complaint status as True when Repair status True #


class RepairFromComplaintApiView(generics.CreateAPIView):
    permission_classes = [permissions.IsAuthenticated]

    def get_installer(self):
        return self.request.user.msduser.get().installer.get()

    def post(self, request):
        pk = request.data['pk']
        complaint = ComplaintModel.objects.get(pk=pk)
        complaint.available = False
        complaint.save()
        repair = RepairModel.objects.create(
            installer=self.get_installer(), complaint=complaint)
        serializer = RepairSerializer(repair)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ComplaintsInstallerApiView(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ComplaintSerializer
    queryset = ComplaintModel.objects.filter(available=True)

    # TODO add installer based filters to queryset #
    def get_queryset(self):
        return super().get_queryset()
