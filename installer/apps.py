from django.apps import AppConfig


class InstallerConfig(AppConfig):
    name = 'installer'

    def ready(self):
        import msdapi.imageSignals
