from django.db import models
from django.contrib.auth.models import User
from authx.models import MSDUser
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
from duser.models import ComplaintModel
from authx.models import UTYPE_CHOICES

UTYPE = UTYPE_CHOICES[1]


class InstallerModel(models.Model):
    msduser = models.ForeignKey(
        MSDUser, related_name='installer', on_delete=models.CASCADE)
    ngo = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # Other Details


class InstallationModel(models.Model):
    installer = models.ForeignKey(
        InstallerModel, blank=True, related_name='installations', on_delete=models.CASCADE)
    date = models.DateField(max_length=255, blank=True, null=True)
    location = models.CharField(
        max_length=255, default='', blank=True, null=True)
    pid = models.CharField(max_length=255, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # OtherFields


class RepairModel(models.Model):
    installer = models.ForeignKey(
        InstallerModel, blank=True, related_name='repairs', on_delete=models.CASCADE)
    date = models.DateField(max_length=255, blank=True,
                            null=True, help_text='Enter in format YYYY-MM-DD')
    location = models.CharField(
        max_length=255, default='', blank=True, null=True)
    pid = models.CharField(max_length=255, blank=True, null=True)
    status = models.BooleanField(default=False, blank=True)
    complaint = models.ForeignKey(
        ComplaintModel, related_name='c_repair', default=None, null=True, on_delete=models.SET_NULL, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # OtherFields


def upload_img(instance, filename):
    i = None
    if instance.installation is not None:
        i = 'installation'
    elif instance.repair is not None:
        i = 'repair'

    if i is not None:
        return '{0}/{1}'.format(i, filename)
    return '{0}'.format(filename)


class InstallerImage(models.Model):
    image = models.ImageField(upload_to=upload_img)
    pid = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, default='reference', blank=True)
    blurValue = models.FloatField(default=0, blank=True)
    lumensValue = models.FloatField(default=0, blank=True)
    allWorking = models.BooleanField(default=False, blank=True)
    detectedLeds = models.FloatField(default=0, blank=True)
    sourceType = models.CharField(
        default='installation', blank=True, max_length=255)
    installation = models.ForeignKey(
        InstallationModel, related_name='installation_images', default=None, null=True, on_delete=models.SET_NULL, blank=True)
    repair = models.ForeignKey(
        RepairModel, related_name='repair_images', default=None, null=True, on_delete=models.SET_NULL, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
