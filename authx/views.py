from django.shortcuts import render
from rest_framework import generics, status, renderers, viewsets
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from rest_framework.permissions import IsAuthenticated
from knox.models import AuthToken
# Create your views here.
from .serializers import LoginSerializer, RegisterSerializer, MSDUserSerializer
from .models import MSDUser


class LoginApi(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data
            return Response({
                "token": AuthToken.objects.create(user)[1],
                "msduser": MSDUserSerializer(user.msduser.get()).data
            })
        except Exception as e:
            return Response({
                'message': e.args[0]['non_field_errors'][0]
            }, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        return Response({'message': 'Get Request'})


class RegisterApi(generics.CreateAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            msduser = serializer.save()
            return Response({
                'status': True,
                'message': 'Registered Successfully',
                'uid': msduser.uid
            })
        except Exception as e:
            return Response({
                'status': False,
                'error': str(e)
            }, status=status.HTTP_400_BAD_REQUEST)


class MSDUserApi(viewsets.ModelViewSet):
    serializer_class = MSDUserSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user.msduser.get()


class CheckLoginApi(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        return Response({'status': True})


class MSDUserSchemaApi(generics.GenericAPIView):

    def get(self, request):
        a = MSDUserSerializer()
        schema = a.fields
        writeable = [{key: value.__str__().split('(')[0].split('Field')[0].lower()}
                     for key, value in schema.items() if not value.read_only]
        readable = [{key: value.__str__().split('(')[0].split('Field')[0].lower()}
                    for key, value in schema.items() if not value.write_only]
        return Response({'readable': readable, 'writeable': writeable})
