from django.db import models
from django.contrib.auth.models import User
# Create your models here.


DUSER = 'DU'
DINSTALLER = 'DI'
DSURVEYOR = 'DS'
DNGO = 'DN'

UTYPE = [DUSER, DINSTALLER, DSURVEYOR, DNGO]

UTYPE_CHOICES = [
    (DUSER, 'DomeUser'),
    (DINSTALLER, 'Installer'),
    (DSURVEYOR, 'Surveyor'),
    (DNGO, 'NGO')
]


class MSDUser(models.Model):
    user = models.ForeignKey(
        User, related_name='msduser', on_delete=models.CASCADE)
    utype = models.CharField(
        max_length=2, choices=UTYPE_CHOICES, default=DUSER)
    uid = models.CharField(max_length=255)
    mobile = models.CharField(
        max_length=255, blank=False, default='00', help_text='Required')
    name = models.CharField(max_length=255, blank=True)
    address = models.CharField(max_length=1024, blank=True)
    village = models.CharField(max_length=255, blank=True)
    taluka = models.CharField(max_length=255, blank=True)
    district = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=255, blank=True)
    pincode = models.CharField(max_length=6, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
