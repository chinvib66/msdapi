from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from .models import MSDUser
from installer.models import InstallerModel
from surveyor.models import SurveyorModel
from duser.models import DomeUserModel
from ngo.models import NGOModel

DUSER = 'DU'
DINSTALLER = 'DI'
DSURVEYOR = 'DS'
DNGO = 'DN'


class MSDUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = MSDUser
        exclude = ['user', 'created_at', 'updated_at']
        read_only_fields = ['utype', 'user', 'uid']


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        try:
            auser = User.objects.get(username=data['username'])
            if not auser.is_active:
                raise serializers.ValidationError(
                    "Account Verification Pending")
            user = authenticate(**data)
            if user:
                return user
        except:
            pass
        raise serializers.ValidationError("Incorrect Credentials")


class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=True, help_text='Required')
    ngo = serializers.CharField(required=False, help_text='Not Required')

    class Meta:
        model = MSDUser
        # fields = '__all__'
        exclude = ['created_at', 'updated_at']
        read_only_fields = ['user', 'uid']
        write_only_fields = ['ngo', 'password']

    def create(self, data):
        uid = data['utype'] + '_' + data['mobile']
        superuser = 0
        staff = 0
        if data['utype'] == DNGO:
            superuser = 1
            staff = 1
        user = User.objects.create_user(
            username=uid, password=data['password'], is_superuser=superuser, is_staff=staff)
        del data['password']
        ngo = ''
        try:
            ngo = data['ngo']
            del data['ngo']
        except KeyError:
            pass
        msduser = MSDUser(**data)
        msduser.user = user
        msduser.uid = uid
        msduser.save()
        if data['utype'] == DINSTALLER:
            installer = InstallerModel.objects.create(msduser=msduser, ngo=ngo)
        elif data['utype'] == DSURVEYOR:
            surveyor = SurveyorModel.objects.create(msduser=msduser, ngo=ngo)
        elif data['utype'] == DUSER:
            duser = DomeUserModel.objects.create(msduser=msduser)
        elif data['utype'] == DNGO:
            ngo = NGOModel.objects.create(msduser=msduser, name=ngo)
        return msduser


# def get_nextautoincrement(mymodel):
#     from django.db import connection
#     cursor = connection.cursor()
#     cursor.execute("SELECT Auto_increment FROM information_schema.tables WHERE table_name='%s' and table_schema='msd';" %
#                    mymodel._meta.db_table)
#     row = cursor.fetchone()
#     cursor.close()
#     return row[0]
