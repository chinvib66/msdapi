from django.urls import path
from .views import LoginApi, RegisterApi, MSDUserApi, CheckLoginApi, MSDUserSchemaApi
from knox.views import LogoutView

app_name = 'authx'

urlpatterns = [
    path('login/', LoginApi.as_view()),
    path('logout/', LogoutView.as_view()),
    path('register/', RegisterApi.as_view()),
    path('user/', MSDUserApi.as_view({
        'get': 'retrieve',
        'patch': 'partial_update'})),
    path('check/', CheckLoginApi.as_view()),
    path('schema/', MSDUserSchemaApi.as_view())
]
