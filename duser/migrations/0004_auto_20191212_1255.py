# Generated by Django 3.0 on 2019-12-12 07:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('duser', '0003_auto_20191212_1228'),
    ]

    operations = [
        migrations.AddField(
            model_name='complaintmodel',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='complaintmodel',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='domeuserimage',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=None),
            preserve_default=False,
        ),
    ]
