from django.db import models
from authx.models import MSDUser
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.
# from installer.models import RepairModel
from authx.models import UTYPE_CHOICES

UTYPE = UTYPE_CHOICES[0]


class DomeUserModel(models.Model):
    msduser = models.ForeignKey(
        MSDUser, related_name='duser', on_delete=models.CASCADE)
    status = models.CharField(max_length=255, default='', blank=True)


class MSD(models.Model):
    duser = models.ForeignKey(DomeUserModel, related_name='domes',
                              on_delete=models.SET_NULL, null=True, blank=True)
    pid = models.CharField(max_length=255, default='', blank=True)
    refBlur = models.FloatField(default=0, blank=True)
    refLumens = models.FloatField(default=0, blank=True)


class ComplaintModel(models.Model):
    duser = models.ForeignKey(
        DomeUserModel, blank=True, related_name='complaints', on_delete=models.CASCADE)
    pid = models.CharField(max_length=255, default='', blank=True)
    title = models.CharField(max_length=255, default='', blank=True)
    message = models.TextField(default='', blank=True)
    status = models.BooleanField(default=False, blank=True)
    available = models.BooleanField(default=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True,)
    updated_at = models.DateTimeField(auto_now=True)

# class FeedbackModel(models.Model):
#     duser = models.ForeignKey(
#         DomeUserModel, blank=True, related_name='feedback', on_delete=models.CASCADE)
#     pid = models.ManyToManyField(MSD, on_delete=models.SET_NULL, null=True)
#     message = models.TextField(blank=True, default='')
    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)


def upload_img(instance, filename):
    i = None
    if instance.complaint is not None:
        i = 'complaint'
    if i is not None:
        return '{0}/{1}'.format(i, filename)
    return '{0}'.format(filename)


class DomeUserImage(models.Model):
    image = models.ImageField(upload_to=upload_img)
    pid = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, default='reference', blank=True)
    blurValue = models.FloatField(default=0, blank=True)
    lumensValue = models.FloatField(default=0, blank=True)
    isBlurred = models.FloatField(default=0, blank=True)
    isQualityGood = models.FloatField(default=0, blank=True)
    allWorking = models.BooleanField(default=False, blank=True)
    detectedLeds = models.FloatField(default=0, blank=True)
    sourceType = models.CharField(
        default='installation', blank=True, max_length=255)
    complaint = models.ForeignKey(
        ComplaintModel, related_name='complaint_images', default=None, null=True, on_delete=models.SET_NULL, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    # feedback = models.ForeignKey(
    #     FeedbackModel, related_name='feedback_images', default=None, null=True, on_delete=models.SET_NULL, blank=True)


@receiver(post_save, sender=DomeUserImage)
def post_save_duimage(sender, instance, created, **kwargs):
    if created:

        return None
