from django.apps import AppConfig


class DuserConfig(AppConfig):
    name = 'duser'

    def ready(self):
        import msdapi.imageSignals
