from django.urls import path

from .views import ComplaintApiView, MSDomeApiView, DomeUserApiView


app_name = 'duser'
urlpatterns = [
    path('', DomeUserApiView.as_view()),
    path('complaints/',
         ComplaintApiView.as_view({'get': 'list', 'post': 'create'})),
    path('complaints/<pk>/',
         ComplaintApiView.as_view({'get': 'retrieve', 'patch': 'partial_update'})),
    path('domes/',
         MSDomeApiView.as_view({'get': 'list', 'post': 'create'})),
    path('domes/<pk>/',
         MSDomeApiView.as_view({'get': 'retrieve', 'patch': 'partial_update'})),
]
