from rest_framework import serializers

from .models import DomeUserModel, MSD, ComplaintModel, DomeUserImage


class DUserImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = DomeUserImage
        # fields = '__all__'
        exclude = ['created_at']


class DomeUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = DomeUserModel
        fields = '__all__'
        read_only = ['status']


class MSDSerializer(serializers.ModelSerializer):
    class Meta:
        model = MSD
        fields = '__all__'


class ComplaintSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()

    class Meta:
        model = ComplaintModel
        # fields = '__all__'
        exclude = ['created_at', 'updated_at', 'available']
        read_only = ['status']

    def get_images(self, obj):
        images = DomeUserImage.objects.filter(complaint=obj)
        serializer = DUserImageSerializer(images, many=True)
        return serializer.data
