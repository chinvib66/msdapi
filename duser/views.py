from django.shortcuts import render
from rest_framework import generics, status, renderers, viewsets, parsers, permissions
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
# Create your views here.

from .serializers import MSDSerializer, DomeUserSerializer, DUserImageSerializer, ComplaintSerializer


class DomeUserApiView(generics.RetrieveAPIView):
    serializer_class = DomeUserSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self):
        msduser = self.request.user.msduser.get()
        if msduser.utype == 'DU':
            return msduser.duser.get()
        return None


class ComplaintApiView(viewsets.ModelViewSet):
    serializer_class = ComplaintSerializer
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = (parsers.MultiPartParser, parsers.JSONParser,
                      parsers.FormParser, parsers.FileUploadParser)

    def get_queryset(self):
        return self.request.user.msduser.get().duser.get().complaints.all()

    def get_duser(self):
        return self.request.user.msduser.get().duser.get()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(duser=self.get_duser())
        files = []
        for i in range(int(request.data['fileCount'])):
            print(request.data['file[{0}]'.format(i)])
            files.append({
                'image': request.data['file[{0}]'.format(i)],
                'pid': request.data['pid'],
                'complaint': serializer.instance.pk
            })
        images = DUserImageSerializer(data=files, many=True)
        images.is_valid(raise_exception=True)
        images.save()
        return Response({'complaints': serializer.data}, status.HTTP_201_CREATED)


class MSDomeApiView(viewsets.ModelViewSet):
    serializer_class = MSDSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return self.request.user.msduser.get().duser.get().domes.all()

    def get_duser(self):
        return self.request.user.msduser.get().duser.get()

    def create(self, request):
        duser = self.get_duser()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(duser=duser)
        return Response({'domes': serializer.data}, status.HTTP_201_CREATED)
