from rest_framework import generics, permissions, viewsets, response, status
from .serializers import SurveySerializer, SurveyorSerializer
from rest_framework.permissions import IsAuthenticated
from msdapi.permissions import IsSurveyor
from django.core.exceptions import PermissionDenied


class SurveyorApi(generics.RetrieveAPIView):
    serializer_class = SurveyorSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self):
        msduser = self.request.user.msduser.get()
        if msduser.utype == 'DS':
            return msduser.surveyor.get()
        raise PermissionDenied


class SurveyApi(viewsets.ModelViewSet):
    serializer_class = SurveySerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return self.request.user.msduser.get().surveyor.get().surveys.all()

    def get_surveyor(self):
        return self.request.user.msduser.get().surveyor.get()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(surveyor=self.get_surveyor())
        return response.Response({'survey': serializer.data}, status.HTTP_201_CREATED)
