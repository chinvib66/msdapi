from django.db import models
from django.contrib.auth.models import User
from authx.models import MSDUser
# Create your models here.

from authx.models import UTYPE_CHOICES

UTYPE = UTYPE_CHOICES[2]


class SurveyorModel(models.Model):
    msduser = models.ForeignKey(
        MSDUser, related_name='surveyor', on_delete=models.CASCADE)
    ngo = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # Other Details


class SurveyModel(models.Model):
    surveyor = models.ForeignKey(
        SurveyorModel, blank=True, related_name='surveys', on_delete=models.CASCADE)
    date = models.DateField(max_length=255, blank=True, null=True)
    location = models.CharField(max_length=255, default='', blank=True)
    pid = models.CharField(max_length=255, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # OtherFields
