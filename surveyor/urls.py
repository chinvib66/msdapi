from django.urls import path

from .views import SurveyApi, SurveyorApi


app_name = 'surveyor'
urlpatterns = [
    path('', SurveyorApi.as_view()),
    path('surveys/',
         SurveyApi.as_view({'get': 'list', 'post': 'create'})),
    path('surveys/<pk>/',
         SurveyApi.as_view({'get': 'retrieve', 'patch': 'partial_update'})),

]
