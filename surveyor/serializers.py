from rest_framework import serializers
from .models import SurveyorModel, SurveyModel


class SurveyorSerializer(serializers.ModelSerializer):
    class Meta:
        model = SurveyorModel
        exclude = ['created_at', 'updated_at']
        read_only_fields = ['id', 'msduser']


class SurveySerializer(serializers.ModelSerializer):
    class Meta:
        model = SurveyModel
        exclude = ['created_at', 'updated_at']
        # read_only_fields = ['id']
