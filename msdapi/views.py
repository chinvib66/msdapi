from rest_framework import generics
from rest_framework.response import Response

from authx.serializers import MSDUserSerializer
from installer.serializers import InstallerSerializer, InstallationSerializer, RepairSerializer
from surveyor.serializers import SurveySerializer, SurveyorSerializer
from duser.serializers import ComplaintSerializer


def schemaDict(serializer):
    a = serializer()
    fields = a.fields
    schema = {}
    for key, value in fields.items():
        schema[key] = {'type': value.__str__().split('(')[0].split(
            'Field')[0].lower(), 'editable': not value.read_only}
    return schema


class FullSchemaApi(generics.GenericAPIView):
    def get(self, request):
        msduser = schemaDict(MSDUserSerializer)
        installer = {
            'installer': schemaDict(InstallerSerializer),
            'installation': schemaDict(InstallationSerializer),
            'repair': schemaDict(RepairSerializer)
        }
        surveyor = {
            'surveyor': schemaDict(SurveyorSerializer),
            'survey': schemaDict(SurveySerializer)
        }
        duser = {
            'complaint': schemaDict(ComplaintSerializer)
        }
        return Response({'schema': {'msduser': msduser, 'installer': installer, 'surveyor': surveyor, 'duser': duser}})
