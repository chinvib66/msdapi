from django.db.models.signals import post_save
from django.dispatch import receiver
import os

from msdapi import image_process

from msdapi.settings import BASE_DIR
from duser.models import DomeUserImage, MSD
from installer.models import InstallerImage

BLUR_THRESHOLD = 0.2
LUMENS_THRESHOLD = 0.2


@receiver(post_save, sender=InstallerImage)
def installer_image(sender, instance, created, **kwargs):
    print('installer signal')
    if created:
        curr_img_path = instance.image.path
        instance.blurValue = image_process.calcBlur(curr_img_path)
        instance.lumensValue = image_process.calcLumensForImage(curr_img_path)
        # TODO ## Save these reference values in MSD object
        instance.allWorking, instance.detectedLeds = image_process.allWorking(
            curr_img_path)
        instance.save()
    return None


@receiver(post_save, sender=DomeUserImage)
def user_image(sender, instance, created, **kwargs):
    print('user signal')
    if created:
        curr_img_path = instance.image.path
        ref_images = InstallerImage.objects.filter(pid=instance.pid)
        if len(ref_images) != 0:
            ref_blur_list = [i.blurValue for i in ref_images]
            ref_lumens_list = [i.lumensValue for i in ref_images]
            ref_blur = float(sum(ref_blur_list))/len(ref_blur_list)
            ref_lumens = float(sum(ref_lumens_list))/len(ref_lumens_list)

            instance.isBlurred, instance.blurValue = image_process.isBlurred(
                curr_img_path, ref_blur, BLUR_THRESHOLD)
            lumensValue = image_process.calcLumensForImage(curr_img_path)
            instance.lumensValue = lumensValue
            instance.isQualityGood = image_process.compareValues(
                lumensValue, ref_lumens, LUMENS_THRESHOLD)
            instance.allWorking, instance.detectedLeds = image_process.allWorking(
                curr_img_path)
        else:
            instance.blurValue = image_process.calcBlur(curr_img_path)
            instance.lumensValue = image_process.calcLumensForImage(
                curr_img_path)
            instance.allWorking, instance.detectedLeds = image_process.allWorking(
                curr_img_path)
        instance.save()
    return None
