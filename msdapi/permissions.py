from rest_framework.permissions import BasePermission

DUSER = 'DU'
DINSTALLER = 'DI'
DSURVEYOR = 'DS'
DNGO = 'DN'


class IsInstaller(BasePermission):
    def has_permission(self, request, view):
        if request.user.msduser.get().utype is DINSTALLER:
            return True
        return False


class IsSurveyor(BasePermission):
    def has_permission(self, request, view):
        if view.request.user.msduser.get().utype is DSURVEYOR:
            return True
        return False


class IsNgo(BasePermission):
    def has_permission(self, request, view):
        if request.user.msduser.get().utype is DNGO:
            return True
        return False


class IsInstaller(BasePermission):
    def has_permission(self, request, view):
        if request.user.msduser.get().utype is DUSER:
            return True
        return False
