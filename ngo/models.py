from django.db import models

# Create your models here.

from authx.models import UTYPE_CHOICES, MSDUser

UTYPE = UTYPE_CHOICES[3]


class NGOModel(models.Model):
    msduser = models.ForeignKey(MSDUser, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
